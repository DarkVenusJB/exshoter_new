// Copyright Epic Games, Inc. All Rights Reserved.

#include "EXShooterGameMode.h"
#include "EXShooterPlayerController.h"
#include "EXShooter/Character/EXShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"

AEXShooterGameMode::AEXShooterGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AEXShooterPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
